/**
 * JavaScript-Class to build a SketchPad with a CANVAS-Object - just like a simple MS Paint ;-)
 * 
 * based on examples from http://perfectionkills.com/exploring-canvas-drawing-techniques/
 * 
 * @author Martin Bergann <martin@coderey.de>
 * @url    https://gitlab.com/coderey/sketchpad
 * 
 * @param string ID ID of <canvas>-HTML-Element
 */
function SketchPad(id)
{
    this.el        = document.getElementById(id);
    this.ctx       = null;
    this.isDrawing = false;
    var self       = this;
    this.line      = [];
    this.lines     = [];
    this.undoStack = [];
    this.changed   = false;
    this.color     = 'rgb(0, 0, 0)';
    this.shadow    = 'rgb(128, 128, 128)';
    this.lineWidth = 5;

    this.initContext = function() {
        var ctx         = this.el.getContext('2d');
        ctx.lineJoin    = ctx.lineCap = 'round';
        ctx.shadowBlur  = 2;

        self.ctx = ctx;
    }

    this.initContext();

    this.hasChanged = function()
    {
        return this.changed;
    }

    this.undo = function()
    {
        var line = self.lines.pop();
        if (line !== undefined) {
            self.undoStack.push(line);
        }
        self.refresh();
    }

    this.redo = function()
    {
        var line = self.undoStack.pop();
        if (line !== undefined) {
            self.lines.push(line);
        }
        self.refresh();
    }

    this.getOffsetLeft = function ( elem )
    {
        var offsetLeft = 0;
        do {
            if ( !isNaN( elem.offsetLeft ) )
            {
                offsetLeft += elem.offsetLeft;
            }
        } while( elem = elem.offsetParent );
        return offsetLeft;
    }

    this.getOffsetTop = function ( elem )
    {
        var offsetTop = 0;
        do {
            if ( !isNaN( elem.offsetTop ) )
            {
                offsetTop += elem.offsetTop;
            }
        } while( elem = elem.offsetParent );
        return offsetTop;
    }

    this.getX = function(e) {
        return e.pageX - self.getOffsetLeft(self.el);
    }

    this.getY = function(e) {
        return e.pageY - self.getOffsetTop(self.el);
    }

    this.el.onmousedown = function(e) {
        self.changed   = true;
        self.isDrawing = true;
        self.line.push({ x: self.getX(e), y: self.getY(e), color: self.color, shadow: self.shadow, lineWidth: self.lineWidth});
    };

    this.el.onmousemove = function(e) {
        if (!self.isDrawing) {
            return;
        }
        self.line.push({ x: self.getX(e), y: self.getY(e) });

        self.refresh();
        self.draw(self.line);
    };

    this.refresh = function()
    {
        self.ctx.clearRect(0, 0, self.el.width, self.el.height);
        self.lines.forEach(function(line) {
            self.draw(line);
        });
    }

    this.draw = function(line)
    {
        var p1 = line[0];
        var p2 = line[1];

        self.ctx.beginPath();
        self.ctx.moveTo(p1.x, p1.y);
        self.ctx.strokeStyle = p1.color;
        self.ctx.shadowColor = p1.shadow;
        self.ctx.lineWidth   = p1.lineWidth;

        for (var i = 1, len = line.length; i < len; i++) {
            // we pick the point between pi+1 & pi+2 as the
            // end point and p1 as our control point
            var midPoint = self.midPointBtw(p1, p2);
            self.ctx.quadraticCurveTo(p1.x, p1.y, midPoint.x, midPoint.y);
            p1 = line[i];
            p2 = line[i+1];
        }
        // Draw last line as a straight line while
        // we wait for the next point to be able to calculate
        // the bezier control point
        self.ctx.lineTo(p1.x, p1.y);
        self.ctx.stroke();
    }

    this.el.onmouseup = function() {
        self.isDrawing = false;
        self.lines.push([...self.line]);
        self.line.length = 0;
    };

    this.clear = function()
    {
        self.changed = true;
        self.line.length = 0;
        self.lines.length = 0;
        self.ctx.clearRect(0, 0, self.el.width, self.el.height);
    }


    this.midPointBtw = function(p1, p2) {
        return {
            x: p1.x + (p2.x - p1.x) / 2,
            y: p1.y + (p2.y - p1.y) / 2
        };
    }

    this.toPng = function() {
        self.changed = false;
        return self.el.toDataURL("image/png");
    }
}
